﻿using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BlogDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel = new MainWindowViewModel();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = viewModel;
        }

        private void Btn_Init_Click(object sender, RoutedEventArgs e)
        {
            ///*
            viewModel.Values = null;
            ObservableCollection<double> temp = new ObservableCollection<double>();
            for (int i = 0; i < Uc_RadarMap.Radials; i++)
            {
                temp.Add(i * 10 + new Random().Next(10, 30));
            }
            viewModel.Values = temp;

            viewModel.ValuesHeighLight = null;
            ObservableCollection<double> tempHeighLight = new ObservableCollection<double>();
            for (int i = 0; i < Uc_RadarMap.Radials; i++)
            {
                tempHeighLight.Add(temp.ToList()[new Random().Next(0, 3)]);
                tempHeighLight.Add(temp.ToList()[new Random().Next(3, Uc_RadarMap.Radials)]);
            }
            viewModel.ValuesHeighLight = tempHeighLight;

            viewModel.Titles = null;
            ObservableCollection<string> tempTitles = new ObservableCollection<string>();
            for (int i = 0; i < Uc_RadarMap.Radials; i++)
            {
                tempTitles.Add($"体质 {i}");
            }
            viewModel.Titles = tempTitles;
            //*/
        }
    }
}