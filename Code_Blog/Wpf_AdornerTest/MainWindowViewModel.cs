﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogDemo
{
    public class MainWindowViewModel:ObservableObject
    {
        #region 雷达图测试

        private ObservableCollection<double> values = new ObservableCollection<double>();
        public ObservableCollection<double> Values
        {
            get { return values; }
            set { SetProperty(ref values, value); }
        }

        private ObservableCollection<double> valuesHeighLight = new ObservableCollection<double>();
        public ObservableCollection<double> ValuesHeighLight
        {
            get { return valuesHeighLight; }
            set { SetProperty(ref valuesHeighLight, value); }
        }

        private int layers = 4;
        public int Layers
        {
            get { return layers; }
            set { SetProperty(ref layers, value); }
        }

        private ObservableCollection<double> layersPercentList = new ObservableCollection<double>();
        public ObservableCollection<double> LayersPercentList
        {
            get { return layersPercentList; }
            set { SetProperty(ref layersPercentList, value); }
        }

        private ObservableCollection<string> titles = new ObservableCollection<string>();
        public ObservableCollection<string> Titles
        {
            get { return titles; }
            set { SetProperty(ref titles, value); }
        }

        #endregion

    }
}
