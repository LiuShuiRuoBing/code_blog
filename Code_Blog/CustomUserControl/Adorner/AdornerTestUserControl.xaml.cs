﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace CustomUserControl.CustomAdorner
{
    /// <summary>
    /// AdornerTestUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class AdornerTestUserControl : UserControl
    {
        public AdornerTestUserControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(Border_Text);
            if (adornerLayer != null)
            {
                Adorner[] adorners = adornerLayer.GetAdorners(Border_Text);
                if (adorners == null || adorners.Count() == 0)
                {
                    adornerLayer.Add(new ThumbBorderAdorner(Border_Text));
                }
            }
        }

        private void Border_Text_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            int tempMargin = 100; 

            double left = Canvas.GetLeft(Border_Text);
            if (left < tempMargin)
            {
                left = tempMargin;
                Canvas.SetLeft(Border_Text, left);
            }

            double top = Canvas.GetTop(Border_Text);
            if (top < tempMargin)
            {
                top = tempMargin;
                Canvas.SetTop(Border_Text, top);
            }

            if (Border_Text.ActualWidth < tempMargin)
                Border_Text.Width = tempMargin;

            if (Border_Text.ActualHeight < tempMargin)
                Border_Text.Height = tempMargin;

            if (left + Border_Text.ActualWidth > Canvas_Main.ActualWidth - tempMargin)
                Border_Text.Width = Canvas_Main.ActualWidth - tempMargin - left;

            if (top + Border_Text.ActualHeight > Canvas_Main.ActualHeight - tempMargin)
                Border_Text.Height = Canvas_Main.ActualHeight - tempMargin - top;
        }
    }
}
